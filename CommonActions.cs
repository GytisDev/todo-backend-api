﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace back_end
{
    public static class CommonActions
    {
        public static string GetUserId(HttpContext context)
        {
            var identity = context.User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                var id = identity.FindFirst("id").Value;

                return id;

            }

            return null;
        }
    }
}
