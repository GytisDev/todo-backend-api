﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.DataContracts.User;
using back_end.Repositories;
using back_end.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {

        private readonly IUserRepository _userRepository;
        private readonly ITokenService _tokenService;
        private readonly IUserService _userService;

        public AccountController(
            IUserRepository userRepository,
            ITokenService tokenService,
            IUserService userService)
        {
            _userRepository = userRepository;
            _tokenService = tokenService;
            _userService = userService;
        }

        // POST api/<controller>
        [HttpPost("register")]
        public async Task<ActionResult> RegisterUser([FromBody] AppUserRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceRequestModel = new UserServiceCreateRequestModel
            {
                Email = model.Email,
                Password = model.Password
            };

            var serviceResponse = await _userService.CreateUser(serviceRequestModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.BadRequest)
            {
                return BadRequest(serviceResponse.Response);
            }

            return Ok();

        }

        [HttpPost("token")]
        public async Task<ActionResult> GetToken([FromBody] TokenRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceRequest = new TokenServiceRequestModel
            {
                Email = model.Email,
                Password = model.Password
            };

            var serviceResponseModel = await _tokenService.GetToken(serviceRequest);

            if (serviceResponseModel.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return Unauthorized();
            }

            if (serviceResponseModel.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            return Ok(serviceResponseModel.Response);

        }

        [Authorize]
        [HttpGet("logout")]
        public ActionResult Logout()
        {
            var userId = CommonActions.GetUserId(HttpContext);

            var serviceResponseModel = _tokenService.Logout(userId);

            if (serviceResponseModel.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            return Ok();
        }

        [HttpPost("refresh")]
        public async Task<ActionResult> Refresh([FromBody] RefreshTokenRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceRequest = new TokenServiceRefreshRequestModel
            {
                RefreshToken = model.RefreshToken
            };

            var serviceResponseModel = await _tokenService.RefreshToken(serviceRequest);

            if (serviceResponseModel.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return Unauthorized();
            }

            if (serviceResponseModel.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            return Ok(serviceResponseModel.Response);

        }

        [Authorize]
        [HttpPost("password")]
        public async Task<ActionResult> ChangePassword([FromBody] UserChangePasswordRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var serviceRequestModel = new UserServiceChangePasswordRequestModel
            {
                UserId = CommonActions.GetUserId(HttpContext),
                CurrentPassword = model.Password,
                NewPassword = model.NewPassword
            };

            var serviceResponse = await _userService.ChangePassword(serviceRequestModel);

            if(serviceResponse.ResponseStatus == ServiceResponseType.BadRequest)
            {
                return BadRequest(serviceResponse.Response);
            }

            if(serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            return Ok();

        }


    }
}
