﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.Services;
using back_end.DataContracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventsGetService _eventsGetService;
        private readonly IEventsCreateService _eventsCreateService;
        private readonly IEventsUpdateService _eventsUpdateService;
        private readonly IEventsRemoveService _eventsRemoveService;

        public EventsController(
            IEventsCreateService eventsCreateService,
            IEventsUpdateService eventsUpdateService,
            IEventsRemoveService eventsRemoveService,
            IEventsGetService eventsGetService)
        {
            _eventsCreateService = eventsCreateService;
            _eventsUpdateService = eventsUpdateService;
            _eventsRemoveService = eventsRemoveService;
            _eventsGetService = eventsGetService;
        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<EventDTO>), 200)]
        public ActionResult Get()
        {
            var serviceRequest = new NotesServiceRequestModel
            {
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _eventsGetService.GetEvents(serviceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            return Ok(serviceResponse.Response);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EventCreateServiceResponseModel), 200)]
        public ActionResult Create([FromBody] EventRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createServiceModel = new EventCreateServiceRequestModel
            {
                Title = model.Title,
                Description = model.Description,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                VerboseLocation = model.VerboseLocation,
                TakesPlaceOn = model.TakesPlaceOn,
                ProjectId = model.ProjectId,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _eventsCreateService.Create(createServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult Update(int id, [FromBody] EventRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateServiceModel = new EventUpdateServiceRequestModel
            {
                Id = id,
                Title = model.Title,
                Description = model.Description,
                Latitude = model.Latitude,
                Longitude = model.Longitude,
                VerboseLocation = model.VerboseLocation,
                TakesPlaceOn = model.TakesPlaceOn,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _eventsUpdateService.Update(updateServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var removeServiceModel = new EventRemoveServiceRequestModel
            {
                Id = id,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _eventsRemoveService.Remove(removeServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);
        }
    }
}