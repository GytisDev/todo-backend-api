﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using back_end.DataContracts;
using back_end.Services;
using Microsoft.AspNetCore.Authorization;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INotesGetService _notesGetService;
        private readonly INotesCreateService _notesCreateService;
        private readonly INotesUpdateService _notesUpdateService;
        private readonly INotesRemoveService _notesRemoveService;

        public NotesController(
            INotesGetService notesGetService,
            INotesCreateService notesCreateService,
            INotesUpdateService notesUpdateService,
            INotesRemoveService notesRemoveService)
        {
            _notesGetService = notesGetService;
            _notesCreateService = notesCreateService;
            _notesUpdateService = notesUpdateService;
            _notesRemoveService = notesRemoveService;
        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<NoteDTO>), 200)]
        public ActionResult Get()
        {
            var serviceRequest = new NoteGetServiceRequestModel
            {
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _notesGetService.GetNotes(serviceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            return Ok(serviceResponse.Response);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(NoteCreateServiceResponseModel), 200)]
        public ActionResult Create([FromBody] NoteRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createServiceModel = new NoteCreateServiceRequestModel
            {
                Description = model.Description,
                ProjectId = model.ProjectId,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _notesCreateService.Create(createServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult Update(int id, [FromBody] NoteRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateServiceModel = new NoteUpdateServiceRequestModel
            {
                Id = id,
                Description = model.Description,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _notesUpdateService.Update(updateServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateServiceModel = new NoteRemoveServiceRequestModel
            {
                Id = id,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _notesRemoveService.Remove(updateServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);
        }
    }
}