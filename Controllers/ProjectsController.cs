﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using back_end.Services;
using back_end.DataContracts;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    public class ProjectsController : Controller
    {

        private readonly IProjectsCreateService _projectsCreateService;
        private readonly IProjectsGetService _projectsGetService;
        private readonly IProjectsUpdateService _projectsUpdateService;
        private readonly IProjectsRemoveService _projectsRemoveService;
        private readonly IProjectsGetByIdService _projectsGetByIdService;

        public ProjectsController(
            IProjectsCreateService projectsCreateService,
            IProjectsGetService projectsGetService,
            IProjectsUpdateService projectsUpdateService,
            IProjectsRemoveService projectsRemoveService,
            IProjectsGetByIdService projectsGetByIdService)
        {
            _projectsCreateService = projectsCreateService;
            _projectsGetService = projectsGetService;
            _projectsUpdateService = projectsUpdateService;
            _projectsRemoveService = projectsRemoveService;
            _projectsGetByIdService = projectsGetByIdService;
        }

        // GET: api/<controller>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(ProjectsContainer), 200)]
        public ActionResult GetProjects()
        {
            var serviceRequest = new ProjectsGetServiceRequestModel
            {
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _projectsGetService.GetProjects(serviceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            return Ok(serviceResponse.Response);

        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(ProjectDTO), 200)]
        public ActionResult GetProjectById(int? id)
        {
            if(id == null)
            {
                return BadRequest();
            }

            var serviceRequestModel = new ProjectGetByIdServiceRequestModel
            {
                Id = (int)id,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _projectsGetByIdService.GetById(serviceRequestModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if(serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(ProjectCreateServiceResponseModel), 200)]
        public ActionResult Create([FromBody] ProjectRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createServiceModel = new ProjectCreateServiceRequestModel
            {
                Title = model.Title,
                Category = model.Category,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _projectsCreateService.Create(createServiceModel);

            if(serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if(serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            return Ok(serviceResponse.Response);

        }



        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(ProjectRemoveServiceResponseModel), 200)]
        public ActionResult Update(int? id, [FromBody] ProjectRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id == null)
            {
                return BadRequest();
            }

            var updateServiceRequest = new ProjectRemoveServiceRequestModel
            {
                Id = (int)id,
                Title = model.Title,
                Category = model.Category,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _projectsUpdateService.Update(updateServiceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id == null)
            {
                return BadRequest();
            }

            var removeServiceRequest = new ProjectsRemoveServiceRequestModel
            {
                Id = (int)id,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _projectsRemoveService.Remove(removeServiceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if(serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);
        }
    }
}
