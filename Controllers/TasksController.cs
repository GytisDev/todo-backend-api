﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using back_end.DataContracts;
using back_end.Services;
using Microsoft.AspNetCore.Authorization;

namespace back_end.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksCreateService _tasksCreateService;
        private readonly ITasksUpdateService _tasksUpdateService;
        private readonly ITasksRemoveService _tasksRemoveService;
        private readonly ITasksGetService _tasksGetService;

        public TasksController(
            ITasksCreateService tasksCreateService,
            ITasksUpdateService taskUpdateService,
            ITasksRemoveService taskRemoveService,
            ITasksGetService tasksGetService)
        {
            _tasksCreateService = tasksCreateService;
            _tasksUpdateService = taskUpdateService;
            _tasksRemoveService = taskRemoveService;
            _tasksGetService = tasksGetService;
        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(IEnumerable<TaskDTO>), 200)]
        public ActionResult Get()
        {
            var serviceRequest = new TaskGetServiceRequestModel
            {
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _tasksGetService.GetTasks(serviceRequest);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            return Ok(serviceResponse.Response);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(TaskCreateServiceResponseModel), 200)]
        public ActionResult Create([FromBody] TaskRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var createServiceModel = new TaskCreateServiceRequestModel
            {
                Title = model.Title,
                Description = model.Description,
                ProjectId = model.ProjectId,
                CompleteGoalOn = model.CompleteGoalOn,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _tasksCreateService.Create(createServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult Update(int id, [FromBody] TaskRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateServiceModel = new TaskUpdateServiceRequestModel
            {
                Id = id,
                Title = model.Title,
                Description = model.Description,
                IsCompleted = model.IsCompleted,
                CompleteGoalOn = model.CompleteGoalOn,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _tasksUpdateService.Update(updateServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);

        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var updateServiceModel = new TaskRemoveServiceRequestModel
            {
                Id = id,
                UserId = CommonActions.GetUserId(HttpContext)
            };

            var serviceResponse = _tasksRemoveService.Remove(updateServiceModel);

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unhandled)
            {
                return StatusCode(500);
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.NotFound)
            {
                return NotFound();
            }

            if (serviceResponse.ResponseStatus == ServiceResponseType.Unauthorized)
            {
                return StatusCode(401);
            }

            return Ok(serviceResponse.Response);
        }

    }
}