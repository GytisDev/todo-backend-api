﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class RefreshTokenRequestModel
    {
        [Required]
        public string RefreshToken { set; get; }
    }
}
