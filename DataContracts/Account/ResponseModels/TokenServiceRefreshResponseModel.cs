﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class TokenServiceRefreshResponseModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
