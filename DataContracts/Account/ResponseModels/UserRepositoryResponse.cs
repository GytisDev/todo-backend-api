﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end
{
    public class UserRepositoryResponseModel
    {
        public bool IsSuccess { get; set; }
        public List<string> ErrorMessages { get; set; }
    }
}
