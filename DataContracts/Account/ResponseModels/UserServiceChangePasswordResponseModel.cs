﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class UserServiceChangePasswordResponseModel
    {
        public List<string> ErrorMessages { get; set; }
    }
}
