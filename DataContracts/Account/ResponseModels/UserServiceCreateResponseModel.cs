﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class UserServiceCreateResponseModel
    {
        public List<string> ErrorMessages { get; set; }

    }
}
