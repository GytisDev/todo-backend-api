﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class UserRefreshToken
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public AppUser User { get; set; }

        public string RefreshToken { get; set; }

        public DateTime ExpiresOn { get; set; }

    }
}
