﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class Event
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime TakesPlaceOn { get; set; }
        public string VerboseLocation { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public bool IsCompleted { get; set; }
        public string UserId { get; set; }
        public AppUser User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
