﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class EventRemoveServiceRequestModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
    }
}
