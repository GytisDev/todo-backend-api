﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class EventRequestModel
    {
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime TakesPlaceOn { get; set; }
        public string VerboseLocation { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public bool IsCompleted { get; set; }
        public int ProjectId { get; set; }
    }
}
