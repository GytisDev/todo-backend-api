﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class Note
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UserId { get; set; }
        public AppUser User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
