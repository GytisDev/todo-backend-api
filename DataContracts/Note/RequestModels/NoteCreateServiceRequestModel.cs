﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class NoteCreateServiceRequestModel
    {
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
    }
}
