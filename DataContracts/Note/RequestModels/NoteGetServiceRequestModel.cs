﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class NoteGetServiceRequestModel
    {
        public string UserId { get; set; }
    }
}
