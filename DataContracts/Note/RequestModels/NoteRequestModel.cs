﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class NoteRequestModel
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public int ProjectId { get; set; }
    }
}
