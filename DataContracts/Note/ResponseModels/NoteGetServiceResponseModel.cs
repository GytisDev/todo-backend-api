﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class NoteGetServiceResponseModel
    {
        public IEnumerable<NoteDTO> Notes { get; set; }
    }
}
