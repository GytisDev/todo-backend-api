﻿using System;
using System.Collections.Generic;

namespace back_end.DataContracts
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public DateTime CreatedOn { get; set; }

        public List<Task> Tasks { get; set; }
        public List<Event> Events { get; set; }
        public List<Note> Notes { get; set; }

        public string UserId { get; set; }
        public AppUser User { get; set; }

        public Project()
        {
            Tasks = new List<Task>();
            Events = new List<Event>();
            Notes = new List<Note>();
        }
    }
}
