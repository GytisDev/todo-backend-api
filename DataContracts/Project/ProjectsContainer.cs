﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class ProjectsContainer
    {
        public IEnumerable<ProjectDTO> Projects { get; set; }
    }
}
