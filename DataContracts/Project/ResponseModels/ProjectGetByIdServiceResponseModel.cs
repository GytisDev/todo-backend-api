﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class ProjectGetByIdServiceResponseModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public DateTime CreatedOn { get; set; }

        public List<TaskDTO> Tasks { get; set; }
        public List<EventDTO> Events { get; set; }
        public List<NoteDTO> Notes { get; set; }
    }
}
