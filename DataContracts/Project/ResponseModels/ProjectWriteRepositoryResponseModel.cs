﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class ProjectWriteRepositoryResponseModel
    {
        public int Id { get; set; }
        public bool IsSuccess { get; set; }
    }
}
