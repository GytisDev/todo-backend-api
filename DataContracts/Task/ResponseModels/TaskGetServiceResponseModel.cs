﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_v3.DataContracts.Task.ResponseModels
{
    public class TaskGetServiceResponseModel
    {
        public IEnumerable<TaskDTO> Tasks { get; set; }
    }
}
