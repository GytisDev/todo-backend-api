﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.DataContracts
{
    public class TaskRemoveServiceResponseModel
    {
        public int Id { get; set; }
    }
}
