﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Repositories
{
    public interface IUserRefreshTokenWriteRepository
    {
        bool CreateRefreshToken(UserRefreshToken model);

        bool RemoveRefreshToken(UserRefreshToken model);

        bool RemoveRefreshTokens(List<UserRefreshToken> tokens);

    }
}
