﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Repositories
{
    public interface IUserRepository
    {
        Task<UserRepositoryResponseModel> Create(UserRepositoryRequestModel model);
        Task<AppUser> FindUserById(string userId);
        Task<UserRepositoryResponseModel> ChangePassword(UserRepositoryChangePasswordRequestModel model);
    }
}
