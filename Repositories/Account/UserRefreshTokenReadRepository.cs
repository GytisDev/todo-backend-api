﻿using back_end.DataContracts;
using back_end.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Repositories
{
    public class UserRefreshTokenReadRepository : IUserRefreshTokenReadRepository
    {
        private readonly AppDbContext _context;

        public UserRefreshTokenReadRepository(AppDbContext context)
        {
            _context = context;
        }

        public UserRefreshToken GetRefreshToken(string refreshToken)
        {
            return _context.RefreshTokens.Where(x => x.RefreshToken == refreshToken).FirstOrDefault();
        }

        public List<UserRefreshToken> GetSavedRefreshTokens(string userId)
        {
            return _context.RefreshTokens.Where(x => x.UserId == userId).ToList();
        }

    }
}
