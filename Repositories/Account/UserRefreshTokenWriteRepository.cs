﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Repositories
{
    public class UserRefreshTokenWriteRepository : IUserRefreshTokenWriteRepository
    {

        private readonly AppDbContext _context;

        public UserRefreshTokenWriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public bool CreateRefreshToken(UserRefreshToken token)
        {
            _context.RefreshTokens.Add(token);

            return _context.TrySaveChanges();
        }

        public bool RemoveRefreshToken(UserRefreshToken token)
        {
            _context.RefreshTokens.Remove(token);

            return _context.TrySaveChanges();
        }

        public bool RemoveRefreshTokens(List<UserRefreshToken> tokens)
        {
            _context.RefreshTokens.RemoveRange(tokens);

            return _context.TrySaveChanges();
        }

    }
}
