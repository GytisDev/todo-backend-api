﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;
using Microsoft.AspNetCore.Identity;


namespace back_end
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<AppUser> _userManager;

        public UserRepository(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<AppUser> FindUserById(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        public async Task<UserRepositoryResponseModel> Create(UserRepositoryRequestModel model)
        {
            var appUser = new AppUser {
                UserName = model.Email,
                Email = model.Email
            };
            var identityResult = await _userManager.CreateAsync(appUser, model.Password);

            if (!identityResult.Succeeded)
            {
                return new UserRepositoryResponseModel
                {
                    IsSuccess = false,
                    ErrorMessages = identityResult.Errors.Select(x => x.Description).ToList()
                };
            }

            return new UserRepositoryResponseModel 
            { 
                IsSuccess = true,
                ErrorMessages = new List<string>()
            };
        }

        public async Task<UserRepositoryResponseModel> ChangePassword(UserRepositoryChangePasswordRequestModel model)
        {
            var identityResult = await _userManager.ChangePasswordAsync(model.User, model.CurrentPassword, model.NewPassword);

            if (!identityResult.Succeeded)
            {
                return new UserRepositoryResponseModel
                {
                    IsSuccess = false,
                    ErrorMessages = identityResult.Errors.Select(x => x.Description).ToList()
                };
            }

            return new UserRepositoryResponseModel
            {
                IsSuccess = true,
                ErrorMessages = new List<string>()
            };

        }


    }
}
