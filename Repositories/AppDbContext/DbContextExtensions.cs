﻿
using Microsoft.EntityFrameworkCore;

namespace back_end.Repositories
{
    public static class DbContextExtensions
    {
        public static bool TrySaveChanges(this DbContext dbContext)
        {
            try
            {
                dbContext.SaveChanges();
                return true;
            }
            catch (DbUpdateException)
            {
                return false;
            }

        }
    }
}
