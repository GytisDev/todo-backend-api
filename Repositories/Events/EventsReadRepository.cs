﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class EventsReadRepository : IEventsReadRepository
    {
        private readonly AppDbContext _context;

        public EventsReadRepository(AppDbContext context)
        {
            _context = context;
        }

        public Event GetEventById(int id)
        {
            return _context.Events.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Event> GetEvents(string userId)
        {
            return _context.Events.Where(x => x.UserId == userId);
        }

    }
}
