﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class EventsWriteRepository : IEventsWriteRepository
    {
        private readonly AppDbContext _context;

        public EventsWriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public EventWriteRepositoryResponseModel Create(Event model)
        {
            _context.Events.Add(model);
            bool isSuccess = _context.TrySaveChanges();

            return new EventWriteRepositoryResponseModel
            {
                Id = model.Id,
                IsSuccess = isSuccess
            };
        }

        public EventWriteRepositoryResponseModel Remove(Event model)
        {
            _context.Events.Remove(model);
            bool isSuccess = _context.TrySaveChanges();

            return new EventWriteRepositoryResponseModel
            {
                Id = model.Id,
                IsSuccess = isSuccess
            };
        }

        public EventWriteRepositoryResponseModel Update(Event model)
        {
            var dbTask = _context.Events.Find(model.Id);

            dbTask.Title = model.Title;
            dbTask.Description = model.Description;
            dbTask.IsCompleted = model.IsCompleted;

            bool isSuccess = _context.TrySaveChanges();

            return new EventWriteRepositoryResponseModel
            {
                Id = dbTask.Id,
                IsSuccess = isSuccess
            };
        }
    }
}
