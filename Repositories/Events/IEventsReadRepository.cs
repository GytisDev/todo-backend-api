﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface IEventsReadRepository
    {
        Event GetEventById(int id);
        IEnumerable<Event> GetEvents(string userId);
    }
}
