﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface IEventsWriteRepository
    {
        EventWriteRepositoryResponseModel Create(Event model);
        EventWriteRepositoryResponseModel Update(Event model);
        EventWriteRepositoryResponseModel Remove(Event model);
    }
}
