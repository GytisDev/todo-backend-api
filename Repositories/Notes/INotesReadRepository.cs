﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface INotesReadRepository
    {
        Note GetNoteById(int id);
        IEnumerable<Note> GetNotes(string userId);
    }
}
