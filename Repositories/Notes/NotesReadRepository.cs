﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class NotesReadRepository : INotesReadRepository
    {
        private readonly AppDbContext _context;

        public NotesReadRepository(AppDbContext context)
        {
            _context = context;
        }

        public Note GetNoteById(int id)
        {
            return _context.Notes.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Note> GetNotes(string userId)
        {
            return _context.Notes.Where(x => x.UserId == userId);
        }
    }
}
