﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class NotesWriteRepository : INotesWriteRepository
    {
        private readonly AppDbContext _context;

        public NotesWriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public NoteWriteRepositoryResponseModel Create(Note note)
        {
            _context.Notes.Add(note);
            bool isSuccess = _context.TrySaveChanges();

            return new NoteWriteRepositoryResponseModel
            {
                Id = note.Id,
                IsSuccess = isSuccess
            };
        }

        public NoteWriteRepositoryResponseModel Remove(Note note)
        {
            _context.Notes.Remove(note);
            bool isSuccess = _context.TrySaveChanges();

            return new NoteWriteRepositoryResponseModel
            {
                Id = note.Id,
                IsSuccess = isSuccess
            };
        }

        public NoteWriteRepositoryResponseModel Update(Note note)
        {
            var dbNote = _context.Notes.Find(note.Id);

            dbNote.Description = note.Description;

            bool isSuccess = _context.TrySaveChanges();

            return new NoteWriteRepositoryResponseModel
            {
                Id = dbNote.Id,
                IsSuccess = isSuccess
            };
        }
    }
}
