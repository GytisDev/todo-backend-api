﻿using System.Collections.Generic;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface IProjectsReadRepository
    {
        IEnumerable<Project> GetProjects(string userId);
        Project GetProjectById(int id);
        Project GetProjectByIdNoItems(int id);
    }
}
