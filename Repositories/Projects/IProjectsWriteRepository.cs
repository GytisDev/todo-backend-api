﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface IProjectsWriteRepository
    {
        ProjectWriteRepositoryResponseModel CreateProject(Project category);
        ProjectWriteRepositoryResponseModel UpdateProject(Project category);
        ProjectWriteRepositoryResponseModel DeleteProject(Project category);
    }
}
