﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using Microsoft.EntityFrameworkCore;

namespace back_end.Repositories
{
    public class ProjectsReadRepository : IProjectsReadRepository
    {
        private readonly AppDbContext _context;

        public ProjectsReadRepository(AppDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Project> GetProjects(string userId) //string userId)
        {
            return _context.Projects.Where(x => x.UserId == userId).ToList();
        }

        public Project GetProjectById(int id)
        {
            return _context.Projects
                .Include(project => project.Tasks)
                .Include(project => project.Notes)
                .Include(project => project.Events)
                .SingleOrDefault(x => x.Id == id);
        }

        public Project GetProjectByIdNoItems(int id)
        {
            return _context.Projects.SingleOrDefault(x => x.Id == id);
        }
    }
}
