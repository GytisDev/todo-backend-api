﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class ProjectsWriteRepository : IProjectsWriteRepository
    {
        private readonly AppDbContext _context;

        public ProjectsWriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public ProjectWriteRepositoryResponseModel CreateProject(Project project)
        {
            _context.Projects.Add(project);
            bool isSuccess = _context.TrySaveChanges();


            return new ProjectWriteRepositoryResponseModel
            {
                Id = project.Id,
                IsSuccess = isSuccess
            };
        }

        public ProjectWriteRepositoryResponseModel DeleteProject(Project project)
        {
            _context.Projects.Remove(project);
            bool isSuccess = _context.TrySaveChanges();

            return new ProjectWriteRepositoryResponseModel
            {
                Id = project.Id,
                IsSuccess = isSuccess
            };

        }

        public ProjectWriteRepositoryResponseModel UpdateProject(Project projectModel)
        {
            var project = _context.Projects.Find(projectModel.Id);

            project.Title = projectModel.Title;
            project.Category = projectModel.Category;

            project.Tasks = projectModel.Tasks;
            project.Events = projectModel.Events;
            project.Notes = projectModel.Notes;

            bool isSuccess = _context.TrySaveChanges();

            return new ProjectWriteRepositoryResponseModel
            {
                Id = project.Id,
                IsSuccess = isSuccess
            };


        }
    }
}
