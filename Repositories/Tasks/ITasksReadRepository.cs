﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface ITasksReadRepository
    {
        Task GetTaskById(int id);
        IEnumerable<Task> GetTasks(string userId);
    }
}
