﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public interface ITasksWriteRepository
    {
        TaskWriteRepositoryResponseModel Create(Task task);
        TaskWriteRepositoryResponseModel Update(Task task);
        TaskWriteRepositoryResponseModel Remove(Task task);
    }
}
