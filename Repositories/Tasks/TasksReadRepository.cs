﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class TasksReadRepository : ITasksReadRepository
    {
        private readonly AppDbContext _context;

        public TasksReadRepository(AppDbContext context)
        {
            _context = context;
        }

        public Task GetTaskById(int id)
        {
            return _context.Tasks.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Task> GetTasks(string userId)
        {
            return _context.Tasks.Where(x => x.UserId == userId);
        }
    }
}
