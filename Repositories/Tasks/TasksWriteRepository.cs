﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Repositories
{
    public class TasksWriteRepository : ITasksWriteRepository
    {

        private readonly AppDbContext _context;

        public TasksWriteRepository(AppDbContext context)
        {
            _context = context;
        }

        public TaskWriteRepositoryResponseModel Create(Task task)
        {
            _context.Tasks.Add(task);
            bool isSuccess = _context.TrySaveChanges();

            return new TaskWriteRepositoryResponseModel
            {
                Id = task.Id,
                IsSuccess = isSuccess
            };
           
        }

        public TaskWriteRepositoryResponseModel Remove(Task task)
        {
            _context.Tasks.Remove(task);
            bool isSuccess = _context.TrySaveChanges();

            return new TaskWriteRepositoryResponseModel
            {
                Id = task.Id,
                IsSuccess = isSuccess
            };

        }

        public TaskWriteRepositoryResponseModel Update(Task task)
        {
            var dbTask = _context.Tasks.Find(task.Id);

            dbTask.Title = task.Title;
            dbTask.Description = task.Description;
            dbTask.IsCompleted = task.IsCompleted;

            bool isSuccess = _context.TrySaveChanges();

            return new TaskWriteRepositoryResponseModel
            {
                Id = dbTask.Id,
                IsSuccess = isSuccess
            };

        }
    }
}
