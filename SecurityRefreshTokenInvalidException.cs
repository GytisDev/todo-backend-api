﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end
{
    public class SecurityRefreshTokenInvalidException : SecurityTokenException
    {
        public SecurityRefreshTokenInvalidException(string message) : base(message)
        {
        }
    }
}
