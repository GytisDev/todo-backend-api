﻿using back_end;
using back_end.DataContracts;
using back_end.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end
{
    public interface ITokenService
    {
        Task<ServiceResponse<TokenServiceRefreshResponseModel>> GetToken(TokenServiceRequestModel model);
        Task<ServiceResponse<TokenServiceRefreshResponseModel>> RefreshToken(TokenServiceRefreshRequestModel model);

        ServiceResponse<TokenServiceRefreshResponseModel> Logout(string userId);
    }
}
