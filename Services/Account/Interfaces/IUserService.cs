﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public interface IUserService
    {
        Task<ServiceResponse<UserServiceCreateResponseModel>> CreateUser(UserServiceCreateRequestModel model);
        Task<ServiceResponse<UserServiceChangePasswordResponseModel>> ChangePassword(UserServiceChangePasswordRequestModel model);
    }
}
