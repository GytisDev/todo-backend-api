﻿using back_end;
using back_end.DataContracts;
using back_end.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using back_end_v3;

namespace back_end.Services
{
    public class TokenService : ITokenService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IUserRefreshTokenReadRepository _userRefreshTokenReadRepository;
        private readonly IUserRefreshTokenWriteRepository _userRefreshTokenWriteRepository;
        private readonly IConfiguration _configuration;
        //private readonly AppDbContext _context;

        private const int EXPIRE_REFRESH_TOKEN_DURATION = 2 * 60 * 60 * 24; // 2 DAYS
        private const int EXPIRE_TOKEN_DURATION =  5 * 60; // 5 MINUTES

        public TokenService(
            UserManager<AppUser> userManager,
            IUserRefreshTokenReadRepository userRefreshTokenReadRepository,
            IUserRefreshTokenWriteRepository userRefreshTokenWriteRepository,
            IConfiguration configuration
            )
        {
            _userManager = userManager;
            _userRefreshTokenReadRepository = userRefreshTokenReadRepository;
            _userRefreshTokenWriteRepository = userRefreshTokenWriteRepository;
            _configuration = configuration;
        }

        public async Task<ServiceResponse<TokenServiceRefreshResponseModel>> GetToken(TokenServiceRequestModel model)
        {
            if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Password))
            {
                return null;
            }

            var userToVerify = await _userManager.FindByNameAsync(model.Email);

            if (userToVerify == null)
            {
                userToVerify = await _userManager.FindByEmailAsync(model.Email);

                if (userToVerify == null)
                {
                    return new ServiceResponse<TokenServiceRefreshResponseModel>
                    {
                        Response = null,
                        ResponseStatus = ServiceResponseType.Unauthorized
                    };
                }

            }

            if (!await _userManager.CheckPasswordAsync(userToVerify, model.Password))
            {
                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var newJwtToken = GenerateToken(userToVerify);
            var newRefreshToken = GenerateRefreshToken();

            bool isSuccess = _userRefreshTokenWriteRepository.CreateRefreshToken(new UserRefreshToken
            {
                UserId = userToVerify.Id,
                RefreshToken = newRefreshToken,
                ExpiresOn = DateTime.UtcNow.AddSeconds(EXPIRE_REFRESH_TOKEN_DURATION)
            });

            return new ServiceResponse<TokenServiceRefreshResponseModel>
            {
                Response = new TokenServiceRefreshResponseModel
                {
                    RefreshToken = newRefreshToken,
                    Token = newJwtToken
                },
                ResponseStatus = isSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }

        private string GenerateToken(AppUser user)
        {
            string signingKey = Environment.GetEnvironmentVariable("SigningKey") == null
                ? _configuration.GetValue<string>("SigningKey")
                : Environment.GetEnvironmentVariable("SigningKey");

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = null,
                Audience = null,
                IssuedAt = DateTime.UtcNow,
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddSeconds(EXPIRE_TOKEN_DURATION),
                Subject = new ClaimsIdentity(new List<Claim> {
                    new Claim("id", user.Id.ToString())
                }
                ),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(signingKey)), SecurityAlgorithms.HmacSha256Signature)
            };
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var jwtToken = jwtTokenHandler.CreateJwtSecurityToken(tokenDescriptor);
            var token = jwtTokenHandler.WriteToken(jwtToken);

            return token;
        }

        public async Task<ServiceResponse<TokenServiceRefreshResponseModel>> RefreshToken(TokenServiceRefreshRequestModel model)
        {

            var savedRefreshToken = _userRefreshTokenReadRepository.GetRefreshToken(model.RefreshToken);


            if (savedRefreshToken == null)
            {
                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            if(savedRefreshToken.ExpiresOn <= DateTime.UtcNow)
            {
                _userRefreshTokenWriteRepository.RemoveRefreshToken(savedRefreshToken);

                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            AppUser user = await _userManager.FindByIdAsync(savedRefreshToken.UserId);

            if(user == null)
            {
                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var newJwtToken = GenerateToken(user);
            var newRefreshToken = GenerateRefreshToken();

            bool isSuccess = _userRefreshTokenWriteRepository.RemoveRefreshToken(savedRefreshToken);

            if (!isSuccess)
            {
                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unhandled
                };
            }

            isSuccess = _userRefreshTokenWriteRepository.CreateRefreshToken(new UserRefreshToken
            {
                UserId = user.Id,
                RefreshToken = newRefreshToken,
                ExpiresOn = DateTime.UtcNow.AddSeconds(EXPIRE_REFRESH_TOKEN_DURATION)
            });

            return new ServiceResponse<TokenServiceRefreshResponseModel>
            {
                Response = new TokenServiceRefreshResponseModel
                {
                    Token = newJwtToken,
                    RefreshToken = newRefreshToken
                },
                ResponseStatus = isSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }

        public ServiceResponse<TokenServiceRefreshResponseModel> Logout(string userId)
        {
            var savedRefreshTokens = _userRefreshTokenReadRepository.GetSavedRefreshTokens(userId);

            if(savedRefreshTokens.Count == 0)
            {
                return new ServiceResponse<TokenServiceRefreshResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Success
                };
            }

            bool isSuccess = _userRefreshTokenWriteRepository.RemoveRefreshTokens(savedRefreshTokens);

            return new ServiceResponse<TokenServiceRefreshResponseModel>
            {
                Response = null,
                ResponseStatus = isSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }

        }


    }
}
