﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userService)
        {
            _userRepository = userService;
        }


        public async Task<ServiceResponse<UserServiceChangePasswordResponseModel>> ChangePassword(UserServiceChangePasswordRequestModel model)
        {
            var user = await _userRepository.FindUserById(model.UserId);

            if(user == null)
            {
                return new ServiceResponse<UserServiceChangePasswordResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            var repositoryRequest = new UserRepositoryChangePasswordRequestModel
            {
                User = user,
                CurrentPassword = model.CurrentPassword,
                NewPassword = model.NewPassword
            };

            var repositoryResponse = await _userRepository.ChangePassword(repositoryRequest);

            return new ServiceResponse<UserServiceChangePasswordResponseModel>
            {
                Response = new UserServiceChangePasswordResponseModel
                {
                    ErrorMessages = repositoryResponse.ErrorMessages
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.BadRequest

            };


        }

        public async Task<ServiceResponse<UserServiceCreateResponseModel>> CreateUser(UserServiceCreateRequestModel model)
        {

            var repositoryRequestModel = new UserRepositoryRequestModel
            {
                Email = model.Email,
                Password = model.Password
            };

            var repositoryResponse = await _userRepository.Create(repositoryRequestModel);

            return new ServiceResponse<UserServiceCreateResponseModel>
            {
                Response = new UserServiceCreateResponseModel
                {
                    ErrorMessages = repositoryResponse.ErrorMessages
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.BadRequest

            };

        }
    }
}
