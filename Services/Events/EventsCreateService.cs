﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class EventsCreateService : IEventsCreateService
    {
        private readonly IEventsWriteRepository _eventsWriteRepository;
        private readonly IEventsReadRepository _eventsReadRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public EventsCreateService(
            IEventsWriteRepository eventsWriteRepository,
            IEventsReadRepository eventsReadRepository,
            IProjectsReadRepository projectsReadRepository
            )
        {
            _eventsWriteRepository = eventsWriteRepository;
            _eventsReadRepository = eventsReadRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<EventCreateServiceResponseModel> Create(EventCreateServiceRequestModel model)
        {
            var project = _projectsReadRepository.GetProjectByIdNoItems(model.ProjectId);

            if (project == null)
            {
                return new ServiceResponse<EventCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if (project.UserId != model.UserId)
            {
                return new ServiceResponse<EventCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var eventModel = new Event
            {
                ProjectId = model.ProjectId,
                Description = model.Description,
                CreatedOn = DateTime.UtcNow,
                Title = model.Title,
                TakesPlaceOn = model.TakesPlaceOn,
                VerboseLocation = model.VerboseLocation,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                IsCompleted = false,
                UserId = model.UserId
            };

            var repositoryResponse = _eventsWriteRepository.Create(eventModel);

            return new ServiceResponse<EventCreateServiceResponseModel>
            {
                Response = new EventCreateServiceResponseModel
                {
                    Id = repositoryResponse.Id,
                    Description = eventModel.Description,
                    Title = eventModel.Title,
                    CreatedOn = eventModel.CreatedOn,
                    TakesPlaceOn = eventModel.TakesPlaceOn,
                    VerboseLocation = eventModel.VerboseLocation,
                    Longitude = eventModel.Longitude,
                    IsCompleted = eventModel.IsCompleted,
                    Latitude = eventModel.Latitude
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
