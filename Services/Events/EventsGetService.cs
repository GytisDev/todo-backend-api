﻿using back_end.DataContracts;
using back_end.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public class EventsGetService : IEventsGetService
    {
        private readonly IEventsReadRepository _eventsReadRepository;

        public EventsGetService(IEventsReadRepository eventsReadRepository)
        {
            _eventsReadRepository = eventsReadRepository;
        }

        public ServiceResponse<IEnumerable<EventDTO>> GetEvents(NotesServiceRequestModel requestModel)
        {
            var events = _eventsReadRepository.GetEvents(requestModel.UserId);

            if (events == null)
            {
                return new ServiceResponse<IEnumerable<EventDTO>>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unhandled
                };
            }

            var eventsDTO = events.Select(x => new EventDTO
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                CreatedOn = x.CreatedOn,
                TakesPlaceOn = x.TakesPlaceOn,
                VerboseLocation = x.VerboseLocation,
                Longitude = x.Longitude,
                Latitude = x.Latitude
            });

            
            return new ServiceResponse<IEnumerable<EventDTO>>
            {
                Response = eventsDTO,
                ResponseStatus = ServiceResponseType.Success
            };
            

        }


    }
}
