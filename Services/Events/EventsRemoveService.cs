﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class EventsRemoveService : IEventsRemoveService
    {
        private readonly IEventsWriteRepository _eventsWriteRepository;
        private readonly IEventsReadRepository _eventsReadRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public EventsRemoveService(
            IEventsWriteRepository eventsWriteRepository,
            IEventsReadRepository eventsReadRepository,
            IProjectsReadRepository projectsReadRepository)
        {
            _eventsWriteRepository = eventsWriteRepository;
            _eventsReadRepository = eventsReadRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<EventRemoveServiceResponseModel> Remove(EventRemoveServiceRequestModel model)
        {
            var dbEvent = _eventsReadRepository.GetEventById(model.Id);

            if (dbEvent == null)
            {
                return new ServiceResponse<EventRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbEvent.UserId != model.UserId)
            {
                return new ServiceResponse<EventRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var repositoryResponse = _eventsWriteRepository.Remove(dbEvent);

            return new ServiceResponse<EventRemoveServiceResponseModel>
            {
                Response = new EventRemoveServiceResponseModel
                {
                    Id = dbEvent.Id
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
