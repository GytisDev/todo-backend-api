﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class EventsUpdateService : IEventsUpdateService
    {
        private readonly IEventsWriteRepository _eventsWriteRepository;
        private readonly IEventsReadRepository _eventsReadRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public EventsUpdateService(
            IEventsWriteRepository eventsWriteRepository,
            IEventsReadRepository eventsReadRepository,
            IProjectsReadRepository projectsReadRepository)
        {
            _eventsWriteRepository = eventsWriteRepository;
            _eventsReadRepository = eventsReadRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<EventUpdateServiceResponseModel> Update(EventUpdateServiceRequestModel model)
        {
            var dbEvent = _eventsReadRepository.GetEventById(model.Id);

            if (dbEvent == null)
            {
                return new ServiceResponse<EventUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if (dbEvent.UserId != model.UserId)
            {
                return new ServiceResponse<EventUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var eventModel = new Event
            {
                Id = model.Id,
                Title = model.Title,
                Description = model.Description,
                IsCompleted = model.IsCompleted,
                Longitude = model.Longitude,
                Latitude = model.Latitude,
                VerboseLocation = model.VerboseLocation,
                TakesPlaceOn = model.TakesPlaceOn
            };

            var repositoryRespose = _eventsWriteRepository.Update(eventModel);

            return new ServiceResponse<EventUpdateServiceResponseModel>
            {
                Response = new EventUpdateServiceResponseModel
                {
                    Id = eventModel.Id,
                    Title = eventModel.Title,
                    Description = eventModel.Description,
                    IsCompleted = eventModel.IsCompleted,
                    VerboseLocation = eventModel.VerboseLocation,
                    Latitude = eventModel.Latitude,
                    Longitude = eventModel.Longitude,
                    TakesPlaceOn = eventModel.TakesPlaceOn
                },
                ResponseStatus = repositoryRespose.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
