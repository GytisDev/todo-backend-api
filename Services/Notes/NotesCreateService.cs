﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class NotesCreateService : INotesCreateService
    {
        private readonly INotesReadRepository _notesReadRepository;
        private readonly INotesWriteRepository _notesWriteRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public NotesCreateService(
            INotesReadRepository notesReadRepository,
            INotesWriteRepository notesWriteRepository,
            IProjectsReadRepository projectsReadRepository
            )
        {
            _notesReadRepository = notesReadRepository;
            _notesWriteRepository = notesWriteRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<NoteCreateServiceResponseModel> Create(NoteCreateServiceRequestModel model)
        {
            var project = _projectsReadRepository.GetProjectByIdNoItems(model.ProjectId);

            if (project == null)
            {
                return new ServiceResponse<NoteCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            if (project.UserId != model.UserId)
            {
                return new ServiceResponse<NoteCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var note = new Note
            {
                ProjectId = model.ProjectId,
                Description = model.Description,
                CreatedOn = DateTime.UtcNow,
                UserId = model.UserId
            };

            var repositoryResponse = _notesWriteRepository.Create(note);

            return new ServiceResponse<NoteCreateServiceResponseModel>
            {
                Response = new NoteCreateServiceResponseModel
                {
                    Id = repositoryResponse.Id,
                    Description = note.Description,
                    CreatedOn = note.CreatedOn
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
