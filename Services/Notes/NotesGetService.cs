﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class NotesGetService : INotesGetService
    {
        private readonly INotesReadRepository _notesReadRepository;

        public NotesGetService(INotesReadRepository notesReadRepository)
        {
            _notesReadRepository = notesReadRepository;
        }


        public ServiceResponse<IEnumerable<NoteDTO>> GetNotes(NoteGetServiceRequestModel requestModel)
        {
            var notes = _notesReadRepository.GetNotes(requestModel.UserId);

            if(notes == null)
            {
                return new ServiceResponse<IEnumerable<NoteDTO>>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unhandled
                };
            }

            var notesDtos = notes.Select(note => new NoteDTO
            {
                Id = note.Id,
                Description = note.Description,
                CreatedOn = note.CreatedOn
            });

            return new ServiceResponse<IEnumerable<NoteDTO>>
            {
                Response = notesDtos,
                ResponseStatus = ServiceResponseType.Success
            };

        }
    }
}
