﻿using back_end.DataContracts;
using back_end.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public class NotesRemoveService : INotesRemoveService
    {
        private readonly INotesReadRepository _notesReadRepository;
        private readonly INotesWriteRepository _notesWriteRepository;

        public NotesRemoveService(
            INotesReadRepository notesReadRepository,
            INotesWriteRepository notesWriteRepository
            )
        {
            _notesReadRepository = notesReadRepository;
            _notesWriteRepository = notesWriteRepository;
        }

        public ServiceResponse<NoteRemoveServiceResponseModel> Remove(NoteRemoveServiceRequestModel model)
        {
            var dbNote = _notesReadRepository.GetNoteById(model.Id);

            if (dbNote == null)
            {
                return new ServiceResponse<NoteRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbNote.UserId != model.UserId)
            {
                return new ServiceResponse<NoteRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var repositoryResponse = _notesWriteRepository.Remove(dbNote);

            return new ServiceResponse<NoteRemoveServiceResponseModel>
            {
                Response = new NoteRemoveServiceResponseModel
                {
                    Id = dbNote.Id
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
