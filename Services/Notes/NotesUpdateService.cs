﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class NotesUpdateService : INotesUpdateService
    {
        private readonly INotesReadRepository _notesReadRepository;
        private readonly INotesWriteRepository _notesWriteRepository;

        public NotesUpdateService(
            INotesReadRepository notesReadRepository,
            INotesWriteRepository notesWriteRepository
            )
        {
            _notesReadRepository = notesReadRepository;
            _notesWriteRepository = notesWriteRepository;
        }

        public ServiceResponse<NoteUpdateServiceResponseModel> Update(NoteUpdateServiceRequestModel model)
        {
            var dbNote = _notesReadRepository.GetNoteById(model.Id);

            if (dbNote == null)
            {
                return new ServiceResponse<NoteUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if (dbNote.UserId != model.UserId)
            {
                return new ServiceResponse<NoteUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var note = new Note
            {
                Id = model.Id,
                Description = model.Description
            };

            var repositoryRespose = _notesWriteRepository.Update(note);

            return new ServiceResponse<NoteUpdateServiceResponseModel>
            {
                Response = new NoteUpdateServiceResponseModel
                {
                    Id = note.Id,
                    Description = note.Description
                },
                ResponseStatus = repositoryRespose.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
