﻿using System;
using System.Collections.Generic;
using back_end.DataContracts;

namespace back_end.Services
{
    public interface IProjectsCreateService
    {
        ServiceResponse<ProjectCreateServiceResponseModel> Create(ProjectCreateServiceRequestModel project);
    }
}
