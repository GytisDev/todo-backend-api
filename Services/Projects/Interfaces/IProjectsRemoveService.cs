﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;

namespace back_end.Services
{
    public interface IProjectsRemoveService
    {
        ServiceResponse<ProjectsRemoveServiceResponseModel> Remove(ProjectsRemoveServiceRequestModel model);
    }
}
