﻿using back_end.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public interface IProjectsUpdateService
    {
        ServiceResponse<ProjectRemoveServiceResponseModel> Update(ProjectRemoveServiceRequestModel model);
    }
}
