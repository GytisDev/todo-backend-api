﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class ProjectsCreateService : IProjectsCreateService
    {

        private readonly IProjectsWriteRepository _projectsWriteRepository;

        public ProjectsCreateService(IProjectsWriteRepository projectsWriteRepository)
        {
            _projectsWriteRepository = projectsWriteRepository;
        }

        public ServiceResponse<ProjectCreateServiceResponseModel> Create(ProjectCreateServiceRequestModel model)
        {
            var project = new Project
            {
                Title = model.Title,
                CreatedOn = DateTime.UtcNow,
                Category = model.Category,
                UserId = model.UserId,
                Tasks = new List<Task>(),
                Notes = new List<Note>(),
                Events = new List<Event>()
            };

            var responseModel = _projectsWriteRepository.CreateProject(project);

            return new ServiceResponse<ProjectCreateServiceResponseModel>
            {
                Response = new ProjectCreateServiceResponseModel
                {
                    Id = project.Id,
                    Title = project.Title,
                    Category = project.Category,
                    CreatedOn = project.CreatedOn
                },
                ResponseStatus = responseModel.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }
    }
}
