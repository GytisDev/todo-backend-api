﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class ProjectsGetByIdService : IProjectsGetByIdService
    {
        private readonly IProjectsReadRepository _projectsReadRepository;

        public ProjectsGetByIdService(IProjectsReadRepository projectsReadRepository)
        {
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<ProjectGetByIdServiceResponseModel> GetById(ProjectGetByIdServiceRequestModel model)
        {
            var project = _projectsReadRepository.GetProjectById(model.Id);

            if(project == null)
            {
                return new ServiceResponse<ProjectGetByIdServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.NotFound,
                    Response = null
                };
            }

            if(project.UserId != model.UserId)
            {
                return new ServiceResponse<ProjectGetByIdServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.Unauthorized,
                    Response = null
                };
            }

            return new ServiceResponse<ProjectGetByIdServiceResponseModel>
            {
                Response = new ProjectGetByIdServiceResponseModel
                {
                    Id = project.Id,
                    Title = project.Title,
                    Category = project.Category,
                    CreatedOn = project.CreatedOn,
                    Tasks = project.Tasks.Select(task => new TaskDTO
                    {
                        Id = task.Id,
                        Title = task.Title,
                        Description = task.Description,
                        CreatedOn = task.CreatedOn,
                        IsCompleted = task.IsCompleted
                    }).ToList(),
                    Events = project.Events.Select(eventModel => new EventDTO
                    {
                        Id = eventModel.Id,
                        Title = eventModel.Title,
                        Description = eventModel.Description,
                        CreatedOn = eventModel.CreatedOn,
                        TakesPlaceOn = eventModel.TakesPlaceOn,
                        VerboseLocation = eventModel.VerboseLocation,
                        IsCompleted = eventModel.IsCompleted,
                        Latitude = eventModel.Latitude,
                        Longitude = eventModel.Longitude
                    }).ToList(),
                    Notes = project.Notes.Select(note => new NoteDTO
                    {
                        Id = note.Id,
                        Description = note.Description,
                        CreatedOn = note.CreatedOn
                    }).ToList()
                },
                ResponseStatus = ServiceResponseType.Success
            };

        }
    }
}
