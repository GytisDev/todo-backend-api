﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.Repositories;
using back_end.DataContracts;

namespace back_end.Services
{
    public class ProjectsGetService : IProjectsGetService
    {

        private readonly IProjectsReadRepository _projectsReadRepository;

        public ProjectsGetService(IProjectsReadRepository projectsReadRepository)
        {
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<ProjectsContainer> GetProjects(ProjectsGetServiceRequestModel model)
        {
            var projectsDTOs = _projectsReadRepository
                .GetProjects(model.UserId)
                .Select(project => new ProjectDTO
            {
                Id = project.Id,
                Title = project.Title,
                Category = project.Category,
                CreatedOn = project.CreatedOn,
                Tasks = project.Tasks.Select(task => new TaskDTO
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description
                }).ToList(),
                Events = project.Events.Select(eventModel => new EventDTO
                {
                    Id = eventModel.Id,
                    Title = eventModel.Title,
                    Description = eventModel.Description,
                    TakesPlaceOn = eventModel.TakesPlaceOn,
                    VerboseLocation = eventModel.VerboseLocation,
                    Longitude = eventModel.Longitude,
                    Latitude = eventModel.Latitude

                }
                ).ToList()

            });

            if(projectsDTOs == null)
            {
                return new ServiceResponse<ProjectsContainer>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unhandled
                };
            } else
            {
                return new ServiceResponse<ProjectsContainer>
                {
                    Response = new ProjectsContainer { Projects = projectsDTOs },
                    ResponseStatus = ServiceResponseType.Success
                };
            }

        }
    }
}
