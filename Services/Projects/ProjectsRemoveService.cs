﻿using back_end.DataContracts;
using back_end.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public class ProjectsRemoveService : IProjectsRemoveService
    {
        private readonly IProjectsWriteRepository _projectsWriteRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public ProjectsRemoveService(
            IProjectsWriteRepository projectsWriteRepository,
            IProjectsReadRepository projectsReadRepository)
        {
            _projectsWriteRepository = projectsWriteRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<ProjectsRemoveServiceResponseModel> Remove(ProjectsRemoveServiceRequestModel model)
        {
            var dbProject = _projectsReadRepository.GetProjectById(model.Id);

            if (dbProject == null)
            {
                return new ServiceResponse<ProjectsRemoveServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbProject.UserId != model.UserId)
            {
                return new ServiceResponse<ProjectsRemoveServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var responseModel = _projectsWriteRepository.DeleteProject(dbProject);

            return new ServiceResponse<ProjectsRemoveServiceResponseModel>
            {
                Response = new ProjectsRemoveServiceResponseModel
                {
                    Id = dbProject.Id
                },
                ResponseStatus = responseModel.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };


        }
    }
}
