﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class ProjectsUpdateService : IProjectsUpdateService
    {
        private readonly IProjectsWriteRepository _projectsWriteRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public ProjectsUpdateService(
            IProjectsWriteRepository projectsWriteRepository,
            IProjectsReadRepository projectsReadRepository)
        {
            _projectsWriteRepository = projectsWriteRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<ProjectRemoveServiceResponseModel> Update(ProjectRemoveServiceRequestModel model)
        {
            var dbProject = _projectsReadRepository.GetProjectById(model.Id);

            if(dbProject == null)
            {
                return new ServiceResponse<ProjectRemoveServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbProject.UserId != model.UserId)
            {
                return new ServiceResponse<ProjectRemoveServiceResponseModel>
                {
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var project = new Project
            {
                Id = model.Id,
                Title = model.Title,
                Category = model.Category
            };

            var responseModel = _projectsWriteRepository.UpdateProject(project);

            return new ServiceResponse<ProjectRemoveServiceResponseModel>
            {
                Response = new ProjectRemoveServiceResponseModel
                {
                    Id = project.Id,
                    Title = project.Title,
                    Category = project.Category
                },
                ResponseStatus = responseModel.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };


        }
    }
}
