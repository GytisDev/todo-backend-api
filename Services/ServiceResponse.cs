﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace back_end.Services
{
    public class ServiceResponse<T>
    {    
        public T Response { set; get; }
        public ServiceResponseType ResponseStatus { set; get; }

    }
}
