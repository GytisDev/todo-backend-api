﻿namespace back_end.Services
{
    public enum ServiceResponseType
    {
        Success,
        NotFound,
        Unhandled,
        BadRequest,
        Unauthorized
    }
}