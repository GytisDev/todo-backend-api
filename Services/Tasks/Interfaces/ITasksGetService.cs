﻿using back_end.DataContracts;
using back_end.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end.Services
{
    public interface ITasksGetService
    {
        ServiceResponse<IEnumerable<TaskDTO>> GetTasks(TaskGetServiceRequestModel requestModel);
    }
}
