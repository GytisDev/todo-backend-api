﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;

namespace back_end.Services
{
    public interface ITasksUpdateService
    {
        ServiceResponse<TaskUpdateServiceResponseModel> Update(TaskUpdateServiceRequestModel model);
    }
}
