﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class TasksCreateService : ITasksCreateService
    {
        private readonly ITasksReadRepository _tasksReadRepository;
        private readonly ITasksWriteRepository _tasksWriteRepository;
        private readonly IProjectsReadRepository _projectsReadRepository;

        public TasksCreateService(
            ITasksReadRepository tasksReadRepository,
            ITasksWriteRepository tasksWriteRepository,
            IProjectsReadRepository projectsReadRepository
            )
        {
            _tasksReadRepository = tasksReadRepository;
            _tasksWriteRepository = tasksWriteRepository;
            _projectsReadRepository = projectsReadRepository;
        }

        public ServiceResponse<TaskCreateServiceResponseModel> Create(TaskCreateServiceRequestModel model)
        {
            var project = _projectsReadRepository.GetProjectByIdNoItems(model.ProjectId);

            if (project == null)
            {
                return new ServiceResponse<TaskCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(project.UserId != model.UserId)
            {
                return new ServiceResponse<TaskCreateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var task = new Task
            {
                ProjectId = model.ProjectId,
                Description = model.Description,
                Title = model.Title,
                IsCompleted = false,
                CreatedOn = DateTime.UtcNow,
                CompleteGoalOn = model.CompleteGoalOn,
                UserId = model.UserId
            };

            var repositoryResponse = _tasksWriteRepository.Create(task);

            return new ServiceResponse<TaskCreateServiceResponseModel>
            {
                Response = new TaskCreateServiceResponseModel
                {
                    Id = repositoryResponse.Id,
                    Title = task.Title,
                    Description = task.Description,
                    CreatedOn = task.CreatedOn,
                    IsCompleted = task.IsCompleted
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }
    }
}
