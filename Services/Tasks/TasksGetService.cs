﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class TasksGetService : ITasksGetService
    {

        private readonly ITasksReadRepository _tasksReadRepository;

        public TasksGetService(ITasksReadRepository tasksReadRepository)
        {
            _tasksReadRepository = tasksReadRepository;
        }

        public ServiceResponse<IEnumerable<TaskDTO>> GetTasks(TaskGetServiceRequestModel requestModel)
        {
            var tasks = _tasksReadRepository.GetTasks(requestModel.UserId);

            if (tasks == null)
            {
                return new ServiceResponse<IEnumerable<TaskDTO>>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unhandled
                };
            }

            var tasksDtos = tasks.Select(x => new TaskDTO
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                IsCompleted = x.IsCompleted,
                CreatedOn = x.CreatedOn
            });

            return new ServiceResponse<IEnumerable<TaskDTO>>
            {
                Response = tasksDtos,
                ResponseStatus = ServiceResponseType.Success
            };

        }
    }
}
