﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class TasksRemoveService : ITasksRemoveService
    {
        private readonly ITasksReadRepository _tasksReadRepository;
        private readonly ITasksWriteRepository _tasksWriteRepository;

        public TasksRemoveService(
            ITasksReadRepository tasksReadRepository,
            ITasksWriteRepository tasksWriteRepository
            )
        {
            _tasksReadRepository = tasksReadRepository;
            _tasksWriteRepository = tasksWriteRepository;
        }

        public ServiceResponse<TaskRemoveServiceResponseModel> Remove(TaskRemoveServiceRequestModel model)
        {
            var dbTask = _tasksReadRepository.GetTaskById(model.Id);

            if(dbTask == null)
            {
                return new ServiceResponse<TaskRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbTask.UserId != model.UserId)
            {
                return new ServiceResponse<TaskRemoveServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var repositoryResponse = _tasksWriteRepository.Remove(dbTask);

            return new ServiceResponse<TaskRemoveServiceResponseModel>
            {
                Response = new TaskRemoveServiceResponseModel
                {
                    Id = dbTask.Id
                },
                ResponseStatus = repositoryResponse.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };
        }
    }
}
