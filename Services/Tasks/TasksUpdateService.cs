﻿using System;
using System.Collections.Generic;
using System.Linq;
using back_end.DataContracts;
using back_end.Repositories;

namespace back_end.Services
{
    public class TasksUpdateService : ITasksUpdateService
    {
        private readonly ITasksReadRepository _tasksReadRepository;
        private readonly ITasksWriteRepository _tasksWriteRepository;

        public TasksUpdateService(
            ITasksReadRepository tasksReadRepository,
            ITasksWriteRepository tasksWriteRepository
            )
        {
            _tasksReadRepository = tasksReadRepository;
            _tasksWriteRepository = tasksWriteRepository;
        }

        public ServiceResponse<TaskUpdateServiceResponseModel> Update(TaskUpdateServiceRequestModel model)
        {
            var dbTask = _tasksReadRepository.GetTaskById(model.Id);

            if(dbTask == null)
            {
                return new ServiceResponse<TaskUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.NotFound
                };
            }

            if(dbTask.UserId != model.UserId)
            {
                return new ServiceResponse<TaskUpdateServiceResponseModel>
                {
                    Response = null,
                    ResponseStatus = ServiceResponseType.Unauthorized
                };
            }

            var task = new Task
            {
                Id = model.Id,
                Title = model.Title,
                Description = model.Description,
                IsCompleted = model.IsCompleted,
                CompleteGoalOn = model.CompleteGoalOn
            };

            var repositoryRespose = _tasksWriteRepository.Update(task);

            return new ServiceResponse<TaskUpdateServiceResponseModel>
            {
                Response = new TaskUpdateServiceResponseModel
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
                    IsCompleted = task.IsCompleted,
                    CompleteGoalOn = task.CompleteGoalOn
                },
                ResponseStatus = repositoryRespose.IsSuccess ? ServiceResponseType.Success : ServiceResponseType.Unhandled
            };

        }
    }
}
