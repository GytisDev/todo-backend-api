﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using back_end.Services;
using Microsoft.AspNetCore.Identity;
using back_end.DataContracts;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using back_end_v3;
using Firebase.Auth;

namespace back_end
{
    public class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            //OAuthOptions = new OAuthAuthorizationServerOptions
            //{
            //    TokenEndpointPath = new Microsoft.Owin.PathString("/token"),
            //    Provider = new OAuthAppProvider(),
            //    AccessTokenExpireTimeSpan = TimeSpan.FromDays(2),
            //    AllowInsecureHttp = false
            //};

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var connectionString = Environment.GetEnvironmentVariable("ConnectionString") == null ? Configuration.GetConnectionString("GoogleCloudSQLConnection") : Environment.GetEnvironmentVariable("ConnectionString");

            services.AddDbContextPool<AppDbContext>( // replace "YourDbContext" with the class name of your DbContext
                options => options.UseMySql(connectionString, // replace with your Connection String
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(8, 0, 17), Pomelo.EntityFrameworkCore.MySql.Infrastructure.ServerType.MySql); // replace with your Server Version and Type
                    }
            ));

            services.AddIdentityCore<AppUser>(o =>
            {
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;

            })
            .AddEntityFrameworkStores<AppDbContext>();

            string signingKey = Environment.GetEnvironmentVariable("SigningKey") == null
                ? Configuration.GetValue<string>("SigningKey")
                : Environment.GetEnvironmentVariable("SigningKey");

            services.AddAuthentication(auth =>
            {
                auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(token =>
            {
                token.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(signingKey)),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero

                };
                token.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        if (context.Exception.GetType() == typeof(SecurityTokenInvalidSignatureException))
                        {
                            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        }
                        return System.Threading.Tasks.Task.CompletedTask;
                    }
                };
            });



            // Services
            services.AddScoped<IProjectsGetService, ProjectsGetService>();
            services.AddScoped<IProjectsCreateService, ProjectsCreateService>();
            services.AddScoped<IProjectsUpdateService, ProjectsUpdateService>();
            services.AddScoped<IProjectsRemoveService, ProjectsRemoveService>();
            services.AddScoped<IProjectsGetByIdService, ProjectsGetByIdService>();

            services.AddScoped<ITasksGetService, TasksGetService>();
            services.AddScoped<ITasksCreateService, TasksCreateService>();
            services.AddScoped<ITasksUpdateService, TasksUpdateService>();
            services.AddScoped<ITasksRemoveService, TasksRemoveService>();

            services.AddScoped<INotesGetService, NotesGetService>();
            services.AddScoped<INotesCreateService, NotesCreateService>();
            services.AddScoped<INotesUpdateService, NotesUpdateService>();
            services.AddScoped<INotesRemoveService, NotesRemoveService>();

            services.AddScoped<IEventsGetService, EventsGetService>();
            services.AddScoped<IEventsCreateService, EventsCreateService>();
            services.AddScoped<IEventsUpdateService, EventsUpdateService>();
            services.AddScoped<IEventsRemoveService, EventsRemoveService>();

            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IUserService, UserService>();

            // Repositories
            services.AddScoped<IProjectsReadRepository, ProjectsReadRepository>();
            services.AddScoped<IProjectsWriteRepository, ProjectsWriteRepository>();

            services.AddScoped<ITasksReadRepository, TasksReadRepository>();
            services.AddScoped<ITasksWriteRepository, TasksWriteRepository>();

            services.AddScoped<INotesReadRepository, NotesReadRepository>();
            services.AddScoped<INotesWriteRepository, NotesWriteRepository>();

            services.AddScoped<IEventsReadRepository, EventsReadRepository>();
            services.AddScoped<IEventsWriteRepository, EventsWriteRepository>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserRefreshTokenReadRepository, UserRefreshTokenReadRepository>();
            services.AddScoped<IUserRefreshTokenWriteRepository, UserRefreshTokenWriteRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
